﻿2. Đánh giá người dùng
-  MSSV: 12110127
-  Tại sao bạn phải dùng website này?
	Đầu tiên, English.net là 1 website học tiếng anh hiệu quả, nó  cung cấp các tài liệu tiếng anh,
hay thông tin khóa học cần thiết và hướng đến các đối tượng cụ thể là các nhóm ngành. Hiện tại 
tôi đang là sv năm 3 ngành công nghệ thông tin nên việc trang bị tiếng anh rất quan trọng đặc 
biệt là tiếng anh dành cho chuyên ngành .Việc sử dụng máy tính thường xuyên nên tôi nghĩ rằng 
việc chọn 1 website học tiếng anh online như thế này là cách hiệu quả để học tiếng anh mà không 
mất thời gian để tìm kiếm tài liệu.  
	Theo tôi thấy mặc dù thiết kế khá đơn giản nhưng website mang lại nhiều nguồn thông tin phong phú 
và đầy đủ cho việc học tiếng anh. Khi sử dụng website  tôi sẽ dùng đến các mục như từ điển để trang 
bị vốn từ vựng, mục này cung cấp đầy đủ từ vựng ngoài ra còn kèm theo hình ảnh và ví dụ rất dễ học; 
các bài test về từ ngữ, ngữ pháp giúp tôi kiểm tra lại những bài đã học; nguồn tài liệu sưu tầm nhiều
cuốn sách hay và  mục chia sẽ tôi có thể chia sẽ nguồn tài liệu của mình cũng như  tham khảo các nguồn 
tài liệu của các thành viên khác. Các khóa học mà website cung cấp giúp tôi kiểm tra được năng lực và 
chọn một khóa học phù hợp với nhu cầu . English.net với nhiều tính năng đáp ứng được như cầu trong việc 
học tiếng anh mà lại dễ dàng truy cập sử dụng, vì vậy  nên tôi quyết đinh sử dụng website này.
