﻿1.Bạn giải thích như thế nào với người dùng và nhà đầu tư:
-  Hi vọng khi bạn sử dụng website của mình, nó có thể đáp ứng được nhu cầu mà bạn đặt ra,
 giúp bạn quản lý thời gian một cách hiệu quả nhất. Trong quá trình sử dụng Website mình 
sẽ đề cập đến các chức năng để bạn có thể thấy được tính hiệu quả của nó
+ Hình thành thời gian biểu : lịch trình công việc và mốc thời gian tương ứng ở trên 
website. Từng mốc thời gian với từng việc cụ thể có thể lưu theo ngày tháng…
+ Ngoài giao diện thiết kế riêng cho 1 người dùng có thể liên kết với lịch trình của 
thành viên trong gia đình , bạn bè …
+ Kết hợp với công cụ hổ trợ chuông báo nhắc việc (alarm clock) + chú thích + thông 
báo + ( các mục giải trí  nhạc, hình ảnh)… . Chức năng báo thức tuỳ theo mốc thời gian
để không ảnh hưởng đến công việc : tiếng chuông  báo thức để chú ý mốc thời gian quan 
trọng , bài nhạc báo thức đến thời gian nghĩ ngơi (ăn uống, sinh hoạt , vui chơi …),
 bảng thông báo nhắc nhở trong lúc làm việc  … thay vì đơn giản là tiếng chuông báo 
thức sẽ gây khó chịu cho người dùng trong nhiều trường hợp.
+ Chức năng đăng nhập để có sửa chữa hay cập nhật lịch trình cho phù hợp, đảm bảo tính
 bảo mật cho thông tin lịch trình cá nhân với các thành viên khác.  Việc liên kết với 
thành viên khác cung cấp thông tin cần thiết giúp hình thành thời gian biểu hiệu quả 
hơn . Thêm các tính năng như nhận xét, đánh giá của người dùng về mức độ hoàn thành, 
tính hiệu quả của cá nhân hay thành viên khác trong từng mốc thời gian ( trong giờ, 
ngày , tháng , …). Các thành viên có thể chia sẽ những cách thức quản lý thời gian, 
 sắp xếp mốc thời gian hợp lý dựa trên thông tin của các thành viên khác 
(gia đình, bạn bè, đồng nghiệp) là những người tác động đến lịch trình của cá nhân
 để tăng thêm tính hiệu quả trong việc quản lý thời gian. 
-	        Ngoài ra các bạn có thể liên hệ với mình ở mục ứng dụng để thông báo 
cho mình khi cần giúp đở hay sự cố gì đó.
            Khi bạn dùng website này sẽ đảm bảo cho bạn những chức năng cần thiết cho 
việc quản lý thời gian của bạn cũng như cung cấp cho bạn nhiều thông tin cần thiết.
		Khi bạn quyêt định đầu tư vào website, chúng ta có thể góp phần 
rộng website đưa đến mọi người 1 trang web hữu ích cho việc quản lý thời gian của 
họ. Từ đó, chúng ta kiếm được những lợi ích từ website mang lại. 
	
 
